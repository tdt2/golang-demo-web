package EventRequestDto

type Event struct {
	Name     string
	Location string
	Slug     string
	Dest     string
}
