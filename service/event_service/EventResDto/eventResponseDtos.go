package EventResDto

import "demo-web/models"

type Event struct {
	ID       int
	Name     string
	Location string
	Slug     string
	ImgUrl   string
	IsActive bool
	Desc     string
	News     []models.News
}
