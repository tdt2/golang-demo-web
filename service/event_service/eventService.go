package event_service

import (
	"demo-web/models"
	"demo-web/service/event_service/EventRequestDto"
	"demo-web/service/event_service/EventResDto"
	"github.com/gin-gonic/gin"
	"net/http"
)

func GetEvents(c *gin.Context) {
	var events []EventResDto.Event
	models.DB.Preload("News").Find(&events)
	c.JSON(http.StatusOK, gin.H{"data": events})
}

func GetEventById(c *gin.Context) {
	var event EventResDto.Event
	if err := models.DB.Where("id=?", c.Param("id")).First(&event).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Event not exist"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": event})
}

func DeleteById(c *gin.Context) {
	var event models.Event
	if err := models.DB.Where("id=?", c.Param("id")).First(&event).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Event not exist"})
		return
	}
	models.DB.Where("id=?", c.Param("id")).Delete(&event)
	c.JSON(http.StatusOK, gin.H{"data": "delete complete"})
}

func CreateEvent(c *gin.Context) {
	var event EventRequestDto.Event
	if err := c.ShouldBindJSON(&event); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var input = models.Event{
		Name:     event.Name,
		Location: event.Location,
		Slug:     models.SetSlug(event.Slug),
		Desc:     event.Dest,
	}

	models.DB.Create(&input)

	c.JSON(http.StatusOK, gin.H{"data": input})

}
