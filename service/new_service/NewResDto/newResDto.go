package NewResDto

import "demo-web/models"

type News struct {
	models.Model
	Title   string
	ImgUrl  string
	Desc    string
	Type    string
	EventID uint
}
