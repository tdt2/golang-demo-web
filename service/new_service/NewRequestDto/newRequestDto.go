package NewRequestDto

type News struct {
	Title   string
	ImgUrl  string
	Desc    string
	Type    string
	EventID uint
}
