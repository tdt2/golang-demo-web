package new_service

import (
	"demo-web/models"
	"demo-web/service/new_service/NewRequestDto"
	"demo-web/service/new_service/NewResDto"
	"github.com/gin-gonic/gin"
	"net/http"
)

func GetNews(c *gin.Context) {
	var news []NewResDto.News
	models.DB.Find(&news)
	c.JSON(http.StatusOK, gin.H{"data": news})

}

func GetNewById(c *gin.Context) {
	var res NewResDto.News
	if err := models.DB.Where("id=?", c.Param("id")).First(&res).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "id does exits"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"data": res})
}

func CreateNew(c *gin.Context) {
	var request NewRequestDto.News
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	var new = models.News{
		Title:   request.Title,
		ImgUrl:  request.ImgUrl,
		Desc:    request.Desc,
		Type:    request.Type,
		EventID: request.EventID,
	}
	models.DB.Create(&new)
	c.JSON(http.StatusOK, gin.H{"data": new})

}

func DeleteNewById(c *gin.Context) {
	var new models.News
	if err := models.DB.Where("id=?", c.Param("id")).First(&new).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "New not exits"})
		return
	}
	models.DB.Where("id=?", c.Param("id")).Delete(&new)
	c.JSON(http.StatusOK, gin.H{"data": "delete complete"})

}
func UpdateNewById(c *gin.Context) {
	var existed NewRequestDto.News
	if err := models.DB.Where("id=?", c.Param("id")).First(&existed).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "New not exist"})
		return
	}
	var update = NewRequestDto.News{
		Title:   existed.Title,
		ImgUrl:  existed.ImgUrl,
		Desc:    existed.Desc,
		Type:    existed.Type,
		EventID: existed.EventID,
	}
	if err := c.ShouldBindJSON(&update); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	models.DB.Where("id=?", c.Param("id")).First(&existed).Updates(&update)
	c.JSON(http.StatusOK, gin.H{"data": update})

}

func GetNewByTitle(c *gin.Context) {
	var new NewResDto.News
	if err := models.DB.Where("title=?", c.Param("title")).First(&new).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Not exist new"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"data": new})
}
