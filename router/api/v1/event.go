package v1

type AddEventForm struct {
	Name     string `form:"name"`
	Location string `form:"location"`
	Desc     string `form:"desc"`
}

//func AddEvent(c *gin.Context) {
//	var (
//		appG = app.Gin{C: c}
//		form AddEventForm
//	)
//	err := c.ShouldBindJSON(form)
//	if err != nil {
//		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
//		return
//	}
//
//	articleService := EventResDto.Event{
//		Name:     form.Name,
//		Location: form.Location,
//		Desc:     form.Desc,
//	}
//
//	if err := articleService.Create(); err != nil {
//		appG.Response(http.StatusInternalServerError, nil)
//		return
//	}
//	appG.Response(http.StatusOK, nil)
//}
