package api

import (
	"demo-web/service/event_service"
	"demo-web/service/new_service"
	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {

	r := gin.Default()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	apiv1 := r.Group("/")
	{

		apiv1.GET("/events", event_service.GetEvents)
		apiv1.GET("/events/:id", event_service.GetEventById)
		apiv1.DELETE("/events/:id", event_service.DeleteById)
		apiv1.POST("/events", event_service.CreateEvent)
		apiv1.GET("/news", new_service.GetNews)
		apiv1.GET("/news/:id", new_service.GetNewById)
		apiv1.POST("/news", new_service.CreateNew)
		apiv1.DELETE("/news/:id", new_service.DeleteNewById)
		apiv1.PUT("/news/:id", new_service.UpdateNewById)
		apiv1.GET("/news/title/:title", new_service.GetNewByTitle)

	}
	return r
}
