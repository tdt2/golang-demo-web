package main

import (
	"demo-web/models"
	"demo-web/router/api"
)

func main() {
	models.GetConnect()
	r := api.InitRouter()
	r.Run(":8080")
}
