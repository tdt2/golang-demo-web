package models

type News struct {
	Model
	Title   string `json:"title"`
	ImgUrl  string `json:"img_url" gorm:"column:img_Url""`
	Rank    string `json:"rank"`
	Desc    string `json:"desc"`
	EventID uint   `json:"event_id" gorm:"column:event_id"`
	Type    string `json:"type"`
}
