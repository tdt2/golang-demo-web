package models

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"time"
)

var DB *gorm.DB

type Model struct {
	ID          uint      `gorm:"primary_key" json:"id"`
	CreatedDate time.Time `gorm:"autoUpdateTime" json:"created_date"`
	UpdatedDate time.Time `gorm:"autoUpdateTime" json:"updated_date"`
}

func GetConnect() {
	connectString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		"root",
		"root",
		"localhost",
		"3306",
		"demoweb",
	)
	var err error
	db, err := gorm.Open(mysql.Open(connectString), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info), // Log câu lệnh sql trong console
	})

	// Xử lý nếu quá trình kết nối với CSDL bị lỗi
	if err != nil {
		panic("Failed to connect database")
	}
	DB = db
}

func SetUp() {

}
