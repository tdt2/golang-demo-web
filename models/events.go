package models

import "strings"

type Event struct {
	Model
	Name     string `json:"name"`
	Location string `json:"location"`
	Slug     string `json:"slug"`
	ImgUrl   string `json:"img_url"`
	IsActive bool   `json:"is_active" gorm:"default:false"`
	Desc     string `json:"desc"`
	News     []News `gorm:"foreignKey:EventID"`
}

func SetSlug(input string) string {
	input = strings.TrimSpace(input)
	input = strings.ToLower(input)
	slug := strings.ReplaceAll(input, " ", "-")
	return slug
}
